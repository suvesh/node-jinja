const { readFileSync, mkdirSync, renameSync, writeFileSync, appendFileSync } = require('fs');
const yaml = require('yamljs');
const _ = require('lodash');
const nunjucks = require('nunjucks');

nunjucks.configure('templates', { autoescape: false });

const yamlFile = readFileSync('./config.yaml', 'utf8');
const config = yaml.parse(yamlFile);
const resources = _.get(config, 'resources', []);

const BASE_DIR_PATH = './build-latest';
const scriptFilePath = `${BASE_DIR_PATH}/deploy.sh`;

try {
  const currPath = BASE_DIR_PATH;
  const newPath = `./build-${new Date().getTime()}`;

  try {
    renameSync(currPath, newPath);
    console.log('Migrated Existing Build Folder');
  } catch (error) {}

  mkdirSync(BASE_DIR_PATH);
  console.log('Created Latest Build Folder');

  resources.forEach((resource) => {
    const { name, namespace, service, deployment, persistentVolumes, configMap } = resource;

    const resourceDirectory = `${BASE_DIR_PATH}/${name}`;

    mkdirSync(resourceDirectory);

    appendFileSync(scriptFilePath, `#---------${name.toUpperCase()}---------\n`);

    if (!(_.get(service, 'enabled') === false)) {
      const data = nunjucks.render('service.njk', resource);
      const filePath = `${resourceDirectory}/service.yaml`;
      writeFileSync(filePath, data);
      const kubectlCommand = `kubectl apply -f ${filePath} -n ${namespace} \n`;
      appendFileSync(scriptFilePath, kubectlCommand);
    }

    if (_.get(configMap, 'createConfig')) {
      const data = nunjucks.render(configMap.configFileTemplateName, resource);
      const filePath = `${resourceDirectory}/${name}ConfigMap.yaml`;
      writeFileSync(filePath, data);
      const kubectlCommand = `kubectl apply -f ${filePath} -n ${namespace} \n`;
      appendFileSync(scriptFilePath, kubectlCommand);
    }

    if (!(_.get(persistentVolumes, 'enabled') === false) && _.get(persistentVolumes, 'createClaim')) {
      const data = nunjucks.render('persistentVolume.njk', resource);
      const filePath = `${resourceDirectory}/persistentVolumes.yaml`;
      writeFileSync(filePath, data);
      const kubectlCommand = `kubectl apply -f ${filePath} -n ${namespace} \n`;
      appendFileSync(scriptFilePath, kubectlCommand);
    }

    if (!(_.get(deployment, 'enabled') === false)) {
      const data = nunjucks.render('deployment.njk', resource);
      const filePath = `${resourceDirectory}/deployment.yaml`;
      writeFileSync(filePath, data);
      const kubectlCommand = `kubectl apply -f ${filePath} -n ${namespace} \n`;
      appendFileSync(scriptFilePath, kubectlCommand);
    }
  });

  console.log('BUILD COMPLETED');
} catch (err) {
  console.error(err);
  console.log('ERR - BUILD ABORTED');
}
